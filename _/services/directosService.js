(function(){
    'use strict';

    app.factory('directosServices', directosServices)

    /** @ngInject */
    function directosServices($http){

        return {
            BaseUrlServicios: "Proxy/getsetws.php",
            Post: Post,
            Get: Get,
            Put: Put
        };

        function Get(Metodo, Callbak) {
            //console.log("--------------------    GET    --------------------");
            //console.log("Metodo");
            //console.log(Metodo);

            var URL = this.BaseUrlServicios + "?" + encriptar(Metodo);
            $http
                .get(URL)
                .then(function(data, status, headers, config) {
                    //console.log("Respuesta get");
                    //console.log(data.data);
                    Callbak(data.data);
                })
                .catch(function(data, status, header, config) {
                    //console.log("Respuesta");
                    //console.log(data);
                    Callbak({ error: 2, Dta: 0, Msg: "" });
                });
        }

        function Post(Metodo, Params, Callbak) {
            $http
                .post(
                    this.BaseUrlServicios,
                    encriptar(
                        JSON.stringify({ Metodo: Metodo, Params: Params })
                    )
                )
                .then(function(data, status, headers, config) {
                    Callbak(data.data);
                })
                .catch(function(data, status, header, config) {
                    Callbak({ error: 2, Dta: 0, Msg: "" });
                });
        }

        function Put(Metodo, Params, Callbak) {
            //console.log("--------------------    PUT    --------------------");
            //console.log("Metodo");
            //console.log(Metodo);
            //console.log("Parametros");
            //console.log(Params);

            $http
                .put(
                    this.BaseUrlServicios,
                    encriptar(
                        JSON.stringify({ Metodo: Metodo, Params: Params })
                    )
                )
                .then(function(data, status, headers, config) {
                    //console.log("Respuesta put");
                    //console.log(data.data);
                    Callbak(data.data);
                })
                .catch(function(data, status, header, config) {
                    //console.log("Respuesta");
                    //console.log(data);
                    Callbak({ error: 2, Dta: 0, Msg: "" });
                });
        }

        function encriptar(obj) {
            return window.btoa(encodeURI(obj));
        }
    }

}());