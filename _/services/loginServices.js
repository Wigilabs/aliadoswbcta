(function(){
    'use strict';

    app.factory('loginService', loginService)

    /** @ngInject */
    function loginService($http){

        return {
            BaseUrlServicios: "Proxy/getsetws.php",
            Post: Post
        };
        function Post(Metodo, Params, Callbak) {

            $http.post(this.BaseUrlServicios, encriptar(JSON.stringify({Metodo:Metodo,Params: Params }))).then(function (data, status, headers, config) {
                    Callbak(data.data);
            }).catch(function (data, status, header, config) {
                Callbak({ error: 2, Dta: 0, Msg: "" })
            });
        }

        function encriptar(obj){
            return window.btoa(encodeURI(obj));
        }
    }

}());