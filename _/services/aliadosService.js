(function() {
    "use strict";

    app.factory("aliadosServices", aliadosServices);

    /** @ngInject */
    function aliadosServices($http) {
        return {
            BaseUrlServicios: "Proxy/getsetws.php",
            Post: Post,
            Get: Get,
            Put: Put
        };

        function Get(Metodo, Callbak) {

            var URL = this.BaseUrlServicios + "?" + encriptar(Metodo);
            $http
                .get(URL)
                .then(function(data, status, headers, config) {
                    Callbak(data.data);
                })
                .catch(function(data, status, header, config) {
                    Callbak({ isError: true, Dta: 0, Msg: "" });
                });
        }

        function Post(Metodo, Params, Callbak) {
            $http
                .post(
                    this.BaseUrlServicios,
                    encriptar(
                        JSON.stringify({ Metodo: Metodo, Params: Params })
                    )
                )
                .then(function(data, status, headers, config) {
                    Callbak(data.data);
                })
                .catch(function(data, status, header, config) {
                    Callbak({ isError: true, Dta: 0, Msg: "" });
                });
        }

        function Put(Metodo, Params, Callbak) {
            $http
                .put(
                    this.BaseUrlServicios,
                    encriptar(
                        JSON.stringify({ Metodo: Metodo, Params: Params })
                    )
                )
                .then(function(data, status, headers, config) {
                    Callbak(data.data);
                })
                .catch(function(data, status, header, config) {
                    Callbak({ isError: true, Dta: 0, Msg: "" });
                });
        }

        function encriptar(obj) {
            return window.btoa(encodeURI(obj));
        }
    }
})();
