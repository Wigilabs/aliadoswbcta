var app = angular.module("myApp", ["ui.router","ui.bootstrap","ngResource","angularSpinner"]);
app.config(['$stateProvider','usSpinnerConfigProvider','$locationProvider','$urlRouterProvider',function($stateProvider,usSpinnerConfigProvider,$locationProvider,$urlRouterProvider) {
    usSpinnerConfigProvider.setTheme('smallRed', {color: '#EE382A', radius: 30});
    usSpinnerConfigProvider.setTheme('smallRedBus', {color: '#EE382A', radius: 3});
    $urlRouterProvider.otherwise('/');

    $stateProvider
     .state('index', {
        url: "/",
        templateUrl: "view/login.html",
        controller:"login"
    })    
    .state('dasboard',{
        url:"/dasboard",
        templateUrl:"view/dasboard.html",
        redirectTo: 'dasboard.aliados'
    })
    .state('dasboard.aliados',{
        url:"/aliados",
        templateUrl:"view/aliados.html",
        controller: "aliados"
    })
    .state('dasboard.directos',{
        url:"/directos",
        templateUrl:"view/directos.html",
        controller: "Directos"
    })
    .state('dasboard.controlCasos',{
      url:"/controlCasos",
      templateUrl:"view/controlCasos.html",
      controller: "controlCasos"
    });
    
    $locationProvider.html5Mode(false);
    
}]).directive('stringToNumber', function() {
    return {
      require: 'ngModel',
      link: function(scope, element, attrs, ngModel) {
        ngModel.$parsers.push(function(value) {
          return '' + value;
        });
        ngModel.$formatters.push(function(value) {
          return parseFloat(value);
        });
      }
    };
  }).filter("startFrom", function() {
    return function(input, start) {
        start = +start; //parse to int
        return input.slice(start);
    };
}).run(function($rootScope,$state) {
    $rootScope.cerrarSesion=function () {
        sessionStorage.clear();
        $state.go('index');
    };
});


