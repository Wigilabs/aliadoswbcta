app.controller("aliados", function($scope, $filter, aliadosServices, $http) {
    $scope.showAliados = false;
    $scope.showAliados1 = false;
    $scope.showAliados2 = false;
    $scope.Invalid = false;
    $scope.InvalidEA = false;
    $scope.InvalidBu = false;
    $scope.showSpinner = true;
    $scope.estado = "Activo";
    $scope.text = "Inhabilitar";
    $scope.prograssing = true;
    $scope.currentPage = 0;
    $scope.pageSize = 10;
    $scope.mensajeError = "";

    $(function() {
        var temp="10"; 
        $("#MySelect").val(temp);
    });
    //---------------------------------GET----------------------------------------------------
    $scope.load = function() {
        aliadosServices.Get(
            "web/administrador/aliados/" + $scope.currentPage + "/" + $scope.pageSize,
            function(data) {
                if (data.isError == false) {
                    $scope.aliado = data.response;
                    $scope.showSpinner = false;
                } else {
                    //alert(data.response);
                    $scope.showSpinner = false;
                    if(data.response != ""){
                        $scope.mensajeError = data.response;
                        $("#modalErrorsv").modal("show");
                    }else{
                        $scope.mensajeError = "Por el momento este servicio no se encuentra disponible.";
                        $("#modalErrorsv").modal("show");
                    }
                }
            }
        );
    };
    $scope.page = function() {
        $scope.load();
        $scope.showSpinner = true;
    };
    $scope.next = function() {
        $scope.currentPage++;
        $scope.showSpinner = true;
        $scope.load();
        console.log($scope.currentPage); 
    };
    
    $scope.back = function() {
        if ($scope.currentPage <= 0) {
        } else {
            $scope.currentPage--;
            $scope.showSpinner = true;
            $scope.load();
            console.log($scope.currentPage); 
        }
        
    };
    //---------------------------------POST-------------------------------------------------------------

    //----------------------------------PUT-------------------------------------------------------------

    //-------------------------------------------------------------------------------------------------
    $scope.showcrear = function() {
        $scope.showAliados1 = true;
        $scope.showAliados = true;
        //console.log($scope.showAliados);
    };
    $scope.cancelar = function() {
        console.log($scope.aliados);
        $scope.Invalid = false;
        $scope.showAliados1 = false;
        $scope.showAliados = false;
        $scope.newAliado = null;
        //console.log($scope.showAliados);
    };
    $scope.cancelar1 = function() {
        $scope.showAliados = false;
        $scope.showAliados2 = false;
    };
    $scope.showedit = function(aliado) {
        $scope.showAliados2 = true;
        $scope.showAliados = true;
        $scope.aliados = aliado;
    };

    $scope.saveUser = function() {
        $scope.InvalidEA = true;
        if ($scope.formE.$invalid === true) {
            return;
        } else {
            console.log($scope.aliados);
            aliadosServices.Put("web/administrador/aliados", $scope.aliados, function(data) {
                if (data.isError == false) {
                    $scope.load();
                    $scope.showAliados2 = false;
                    $scope.showAliados = false;
                    $("#modaleditAliado").modal("show");
                } else {
                    //alert(data.response);
                    if(data.response != ""){
                        $scope.mensajeError = data.response;
                        $("#modalError").modal("show");
                    }else{
                        $scope.mensajeError = "Por el momento este servicio no se encuentra disponible.";
                        $("#modalError").modal("show");
                    }
                }
            });
        }
        $scope.InvalidEA = false;
    };
    $scope.Nuevo = function() {
        $scope.Invalid = true;
        if ($scope.formu.$invalid === true) {
            return;
        } else {
            console.log($scope.newAliado);
            aliadosServices.Post("web/administrador/aliados",$scope.newAliado,function (data) {
                if(data.isError==false){
                    $scope.load();
                    $scope.newAliado = null;
                    $scope.showAliados1 = false;
                    $scope.showAliados = false;
                    $("#modalAliado").modal("show");
                    $scope.Invalid = false;
                }else{
                    //alert(data.response);
                    if(data.response != ""){
                        $scope.mensajeError = data.response;
                        $("#modalError").modal("show");
                    }else{
                        $scope.mensajeError = "Por el momento este servicio no se encuentra disponible.";
                        $("#modalError").modal("show");
                    }
                }
            });     
        }
    };
    $scope.Buscar = function(){
        $scope.InvalidBu = true;
        if ($scope.formuBus.$invalid === true) {
            return;
        } else {
            console.log($scope.buscar);
            $scope.showSpinnerBus = true;
            aliadosServices.Post("web/buscadorAdmin/aliados",$scope.buscar,function (data) {
                if(data.isError==false){
                    $scope.aliado = data.response;
                    $scope.showSpinnerBus = false;
                    $scope.buscar = null;
                    $scope.InvalidBu = false;
                }else{
                    if(data.response != ""){
                        $scope.mensajeError = data.response;
                        $("#modalErrorsv").modal("show");
                    }else{
                        $scope.mensajeError = "Por el momento este servicio no se encuentra disponible.";
                        $("#modalErrorsv").modal("show");
                    }
                    $scope.showSpinnerBus = false;
                    
                }
            });    
                
        }
    }
    $scope.reload = function() {
        location.reload();
    };
    $scope.modalhabIopen = function() {
        $("#modalDesAliado").modal("show");
    };
    $scope.modalinIopen = function() {
        $("#modalhabAliado").modal("show");
    };

    $scope.getaliados = function() {
        return $filter("filter")($scope.aliado, $scope.search);
    };
    $scope.numberOfPages = function() {
        return Math.ceil($scope.getaliados().length / $scope.pageSize);
    };

    $scope.$watchGroup(
        "search",
        function(newValue, oldValue) {
            if (oldValue != newValue) {
                $scope.currentPage = 0;
            }
        },
        true
    );
    $scope.validarSesion=function(){
        if(sessionStorage.Cookie){
            $scope.load();
        }else{
            window.location.href="/";
        }
    };
    $scope.validarSesion();
});
