app.controller("login", function($scope,loginService,$rootScope,$state) { 
    $scope.invalidlog =  false;
    $scope.loginIn = false;
    $scope.tipo = 'password';
    $scope.login = {correo : "" , clave :""};
    $scope.mensajeError = "";
    $scope.showPass = function(){
        if($scope.tipo === 'password'){
            $scope.tipo = 'text';
            $scope.toggle = !$scope.toggle;
        }else{
            $scope.tipo = 'password';
            $scope.toggle = !$scope.toggle;
        }
    }
    $scope.validateForm = function(){
        $scope.invalidlog =  true;
        if($scope.formLogin.$invalid === true){
            return;
        }else{
            loginService.Post("web/administrador/login",$scope.login,function(data){
                if(data.isError==false){
                    sessionStorage.Cookie=window.btoa(data.response);
                    $state.go("dasboard");
                }else{
                    $scope.loginIn = true;
                    $scope.mensajeError = data.response;
                    $("#modalErrorLogin").modal("show");
                } 
            });
        }
    }
});


