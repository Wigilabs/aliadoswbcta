app.controller("Directos", function($scope, $filter, directosServices) {
    $scope.showDirectos = false;
    $scope.showDirectos1 = false;
    $scope.showDirectos2 = false;
    $scope.InvalidBu = false;
    $scope.busDiv = false;
    $scope.busHide= false;
    //$scope.directo = [];
    $scope.Invalid = false;
    $scope.InvalidED = false;
    $scope.showSpinner = true;
    $scope.currentPage = 0;
    $scope.pageSize = 10;
    $scope.mensajeError = "";

    $(function() {
        var temp="10"; 
        $("#MySelect").val(temp);
    });
    //---------------------------------------------------------------------------------------
    $scope.load = function() {
        directosServices.Get(
            "web/administrador/directos/" + $scope.currentPage + "/" + $scope.pageSize,
            function(data) {
                if (data.isError == false) {
                    $scope.directo = data.response;
                    $scope.showSpinner = false;
                } else {
                    //alert(data.response);
                    $scope.showSpinner = false;
                    if(data.response != ""){
                        $scope.mensajeError = data.response;
                        $("#modalErrorsv").modal("show");
                    }else{
                        $scope.mensajeError = "Por el momento este servicio no se encuentra disponible.";
                        $("#modalErrorsv").modal("show");
                    }
                    
                }
            }
        );
    };
    
    $scope.page = function() {
        $scope.showSpinner = true;
        $scope.load();
    };

    $scope.next = function() {
        $scope.currentPage++;
        $scope.showSpinner = true;
        $scope.load();
        console.log($scope.currentPage);
    };
    $scope.back = function() {
        if ($scope.currentPage <= 0) {
        } else {
            $scope.currentPage--;
            console.log($scope.currentPage);
            $scope.showSpinner = true;
            $scope.load();
            
        }
    };
    $scope.showcrear = function() {
        $scope.showDirectos1 = true;
        $scope.showDirectos = true;
        console.log($scope.showDirectos);
    };
    $scope.cancelar = function() {
        $scope.newDirecto = null;
        $scope.Invalid = false;
        $scope.showDirectos1 = false;
        $scope.showDirectos = false;
    };
    $scope.cancelar1 = function() {
        $scope.showDirectos = false;
        $scope.showDirectos2 = false;
        $scope.InvalidED = false;
    };
    $scope.showedit = function(directo) {
        $scope.showDirectos2 = true;
        $scope.showDirectos = true;
        $scope.directos = directo;
        /*console.log($scope.current);*/
        $scope.saveUser = function() {
            $scope.InvalidED = true;
            if ($scope.formED.$invalid === true) {
                return;
            } else {
                directosServices
                    .Put("web/administrador/directos",$scope.directos,function(data){
                        if(data.isError==false){
                            $scope.load();
                            $scope.showDirectos = false;
                            $scope.showDirectos1 = false;
            
                            $("#modaleditDirecto").modal("show");
                        }else{
                            //alert(data.response);
                            if(data.response != ""){
                                $scope.mensajeError = data.response;
                                $("#modalError").modal("show");
                            }else{
                                $scope.mensajeError = "Por el momento este servicio no se encuentra disponible.";
                                $("#modalError").modal("show");
                            }
                            
                        }
                    });
                    
                       
            }
            $scope.InvalidED = false;
        };
    };

    $scope.modalIHabopen = function(directo) {
        $scope.directos = directo;
        $("#modalDesDirecto").modal("show");
    };
    $scope.habDirectos = function() {
        $scope.directos.activo = "false";
        console.log($scope.directos);
        directosServices.Put("web/administrador/directos",$scope.directos,function(data) {
            if(data.isError==false){
                $scope.load();
            }else{
                //alert(data.response);
                if(data.response != ""){
                    $scope.mensajeError = data.response;
                    $("#modalError").modal("show");
                }else{
                    $scope.mensajeError = "Por el momento este servicio no se encuentra disponible.";
                    $("#modalError").modal("show");
                }
            }
        });
    };
    $scope.modalIopen = function(directo) {
        $scope.directos = directo;
        $("#modalHabDirecto").modal("show");
    };
    $scope.desDirectos = function() {
        $scope.directos.activo = "true";
        console.log($scope.directos);
        directosServices.Put("web/administrador/directos",$scope.directos,function(data) {
            if(data.isError==false){
                $scope.load();
            }else{
                //alert(data.response);
                if(data.response != ""){
                    $scope.mensajeError = data.response;
                    $("#modalError").modal("show");
                }else{
                    $scope.mensajeError = "Por el momento este servicio no se encuentra disponible.";
                    $("#modalError").modal("show");
                }
            }
        });
    };
    $scope.NuevoDir = function() {
        $scope.Invalid = true;
        if ($scope.formuD.$invalid === true) {
            return;
        } else {
            directosServices
                .Post("web/administrador/directos",$scope.newDirecto,function(data){
                    if(data.isError==false){
                        $scope.load();
                        $scope.showDirectos = false;
                        $scope.showDirectos1 = false;
                        $scope.newDirecto = null;
                        $("#modalDirecto").modal("show");
                    }else{
                        //alert(data.response);
                        if(data.response != ""){
                            $scope.mensajeError = data.response;
                            $("#modalError").modal("show");
                        }else{
                            $scope.mensajeError = "Por el momento este servicio no se encuentra disponible.";
                            $("#modalError").modal("show");
                        }
                    }
                });
                   
        }
        $scope.Invalid = false;
    };
    $scope.Buscar = function(){
        $scope.InvalidBu = true;
        if ($scope.formuBusD.$invalid === true) {
            return;
        } else {
            console.log($scope.buscar);
            $scope.showSpinnerBus = true;
            directosServices.Post("web/buscadorAdmin/directos",$scope.buscar,function (data) {
                if(data.isError==false){
                    $scope.busDiv = true;
                    $scope.busHide= true;
                    $scope.currentPage = 0;
                    $scope.pageSize = 10;
                    $scope.directo = data.response;
                    $scope.showSpinnerBus = false;
                    $scope.buscar = null;
                    $scope.InvalidBu = false;
                }else{
                    //alert(data.response);
                    $scope.showSpinnerBus = false;
                    if(data.response != ""){
                        $scope.mensajeError = data.response;
                        $("#modalErrorsv").modal("show");
                    }else{
                        $scope.mensajeError = "Por el momento este servicio no se encuentra disponible.";
                        $("#modalErrorsv").modal("show");
                    }
                   
                }
            });    
                
        }
    }
    $scope.reload = function() {
        location.reload();
    };
    $scope.getdirectos = function() {
        return $filter("filter")($scope.directo, $scope.search);
    };

    $scope.numberOfPages = function() {
        return Math.ceil($scope.getdirectos().length / $scope.pageSize);
    };

    $scope.$watchGroup(
        "search",
        function(newValue, oldValue) {
            if (oldValue != newValue) {
                $scope.currentPage = 0;
            }
        },
        true
    );

    $scope.validarSesion=function(){
        if(sessionStorage.Cookie){
            $scope.load();
        }else{
            window.location.href="/";
        }
    };
    $scope.validarSesion();

    $scope.getdirectos = function () {
    
        return $filter('filter')($scope.directo);    
        }
    
        $scope.numberOfPages=function(){
            
        return Math.ceil($scope.getdirectos().length/$scope.pageSize);
        
        }
        
        $scope.$watchGroup('search', function(newValue,oldValue){
        if(oldValue!=newValue){
        $scope.currentPage = 0;
        }
        },true);

});
app.filter('startFrom', function() {
    return function(input, start) {
    start = +start;
    return input.slice(start);
    }
});


