app.controller("controlCasos", function($scope, ControlCasosService) { 
    $scope.dias = 0; 
    $scope.load = function() {
        ControlCasosService.Get(
            "movil/dias",
            function(data) {
                if (data.isError == false) {
                    let response = data.response;
                    $scope.dias = response.cantidad;
                } else {
                    //alert(data.response);
                    if(data.response != ""){
                        $scope.mensajeError = data.response;
                        $("#modalErrorsv").modal("show");
                    }else{
                        $scope.mensajeError = "Por el momento este servicio no se encuentra disponible.";
                        $("#modalErrorsv").modal("show");
                    }
                    
                }
            }
        );
    };

    $scope.validarSesion=function(){
        if(sessionStorage.Cookie){
            $scope.load();
        }else{
            window.location.href="/";
        }
    };
    $scope.validarSesion();

    $scope.actualizarDias = function() {
        let objData = {
            "cantidad":$scope.dias
        }
        ControlCasosService.Put(
            "movil/dias",objData,
            function(data) {
                if (data.isError == false) {
                    $("#diasActualizados").modal("show");
                    $scope.load();
                } else {
                    //alert(data.response);
                    if(data.response != ""){
                        $scope.mensajeError = data.response;
                        $("#modalErrorsv").modal("show");
                    }else{
                        $scope.mensajeError = "Por el momento este servicio no se encuentra disponible.";
                        $("#modalErrorsv").modal("show");
                    }
                    
                }
            }
        );
    }
})