<?php

//Produccion
$URLGLOBAL = "https://claroteayuda.claro.com.co:8200/CTA/api/v1/";

//QA
//$URLGLOBAL = "http://claroteayudaqa.claro.com.co:8200/CTA/api/v1/";


if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    $url = $_SERVER["REQUEST_URI"];
    $url = explode("?", $url);
    $urlextra = base64_decode($url[1]);

    // Get cURL resource
    $curl = curl_init();
    // Set some options - we are passing in a useragent too here
    curl_setopt_array($curl, array(
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_URL => $URLGLOBAL . $urlextra,
        CURLOPT_USERAGENT => 'Codular Sample cURL Request',
    ));

    // Send the request & save response to $resp
    $server_output = curl_exec($curl);
    // Close request to clear up some resources
    curl_close($curl);
    echo $server_output;

} else {
    $origen = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : 'algo';
    $origen = explode(":", $origen)[0];
    header("Access-Control-Allow-Origin:" . $origen);
    header('Access-Control-Allow-Headers: Content-Type');
    $request = file_get_contents('php://input');

    $params = json_decode(urldecode(base64_decode($request)), true);

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $URLGLOBAL . $params["Metodo"]);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $_SERVER['REQUEST_METHOD']);
    if($params["Metodo"] == "movil/dias"){
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params["Params"]));
    }else{
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode(array('data' => $params["Params"])));
    }
    

    $x = array();
    array_push($x, 'Content-Type: application/json');
    curl_setopt($ch, CURLOPT_HTTPHEADER, $x);

    $server_output = curl_exec($ch);

    curl_close($ch);

    echo $server_output;
}
